public class AmplitudeTracker extends Chubgraph
{
  // patch
  64 => int npts;
  Sigmund freud;
  freud.npts(npts);
  freud.maxfreq(3000);
  inlet => freud => blackhole;

  Step dc;
  1 => dc.next;
  Envelope env;
  0 => env.value;
  npts::samp => env.duration;
  dc => env => outlet;

  // spork
  spork ~ amplitudetrack_process();

  fun void amplitudetrack_process(){
    while (true){
      freud.env() / 100.0 => env.target;
      npts::samp => now;
    }
  }
}
