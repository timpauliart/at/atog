public class WetRack extends Chubgraph
{
    // status
    16 => float shift;
    1 => int patch;

    // mix
    Gain mix;
    0.5 => mix.gain;
    mix => outlet;

    // fft
    FFTwave fftwave;
    Gain switcher;
    0 => switcher.gain;
    Gain multiply;
    3 => multiply.op;
    Gain g;
    inlet => fftwave => multiply => switcher => g => mix;

    // rev
    GVerb rev;
    0 => rev.dry;
    Gain g_rev;
    switcher => rev => g_rev => mix;

    // track
    PitchTrack pt;
    pt.frame(512);
    inlet => pt => blackhole;
    AmplitudeTracker at;
    inlet => at => multiply;

    // sine
    SinOsc sine;
    Gain sine_switcher;
    0 => sine_switcher.gain;
    Gain sine_multiply;
    3 => sine_multiply.op;
    Envelope env;
    0 => env.value;
    sine => sine_multiply => env => sine_switcher => g;
    AmplitudeTracker sine_at;
    multiply => sine_at => sine_multiply;

    patch1();

    spork ~ pitch_process();

    fun void pitch_process(){
      while (true){
        pt.get() => float realfreq;
        realfreq * shift => float freq;
        fftwave.setFreq(freq);
        Utility.find_octave(realfreq, 40, 60) => float sinefreq;
        if (at.freud.freq() == 0 || sinefreq == 0){
          (env.value())::second => env.duration;
          0 => env.target;
        }
        else{
          sinefreq => sine.freq;
          (env.value())::second => env.duration;
          1 => env.target;
        }
        64::samp => now;
      }
    }

    // print
    fun void print_patch(int i){
      i => patch;
      <<<"patch", i>>>;
    }

    fun void patch1(){
      fftwave.setPhaseOn(0);
      8 => shift;
      // rev
      300 => rev.roomsize;
      8.3::second => rev.revtime;
      0.2 => rev.damping;
      0.2 => rev.early;
      0.8 => rev.tail;
      //
      0 => sine.op;
      0 => sine_at.op;

      print_patch(1);
    }

    fun void patch2(){
      fftwave.setPhaseOn(1);
      4 => shift;
      // rev
      250 => rev.roomsize;
      8.3::second => rev.revtime;
      0.4 => rev.damping;
      0.4 => rev.early;
      0.6 => rev.tail;
      //
      0 => sine.op;
      0 => sine_at.op;

      print_patch(2);
    }

    fun void patch3(){
      fftwave.setPhaseOn(0);
      (1 / 16.0) => shift;
      // rev
      200 => rev.roomsize;
      4.1::second => rev.revtime;
      0.6 => rev.damping;
      0.6 => rev.early;
      0.4 => rev.tail;
      //
      1 => sine.op;
      1 => sine_at.op;

      print_patch(3);
    }

    fun void patch4(){
      fftwave.setPhaseOn(1);
      (1 / 16.0) => shift;
      // rev
      150 => rev.roomsize;
      4.1::second => rev.revtime;
      0.8 => rev.damping;
      0.8 => rev.early;
      0.2 => rev.tail;
      //
      1 => sine.op;
      1 => sine_at.op;

      print_patch(4);
    }

    fun void patch5(){
      fftwave.setPhaseOn(1);
      (1 / 4.0) => shift;
      // rev
      100 => rev.roomsize;
      8.3::second => rev.revtime;
      0.5 => rev.damping;
      0.5 => rev.early;
      0.5 => rev.tail;
      //
      1 => sine.op;
      1 => sine_at.op;

      print_patch(5);
    }
}
