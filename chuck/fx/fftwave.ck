public class FFTwave extends Chubgraph
{
    // signal flow graph
    FFT fft;
    inlet => fft => blackhole;
    Wavetable wt;
    wt => outlet;

    // real attributes
    44100 => int maxsize;
    complex fftresult[maxsize];
    float waveresult[maxsize];
    maxsize => int realBuffersize;
    0 => int realPhaseon;
    // future attributes
    512 => int futureBuffersize;
    float futureFreq;

    // spork
    spork ~ fftwave_process();

    fun void fftwave_process(){
      while (true){
        if (futureBuffersize != realBuffersize){
          updateFreq();
        }
        fft.upchuck().cvals() @=> fftresult;
        for(0 => int i; i < realBuffersize; i++){
          ffttodac(fftresult[i]) => waveresult[i];
        }
        1.0 / Utility.maxabs(waveresult) => float scale;
        for(0 => int i; i < realBuffersize; i++){
          waveresult[i] * scale => waveresult[i];
        }
        realBuffersize::samp => now;
      }
    }

    fun float ffttodac(complex cmp){
      cmp$polar => polar tmp;
      float result;
      if (realPhaseon){
        tmp.phase / pi => result;
      }
      else {
        tmp.mag => result;
      }
      return result;
    }

    fun void setFreq(float freq){
      freq => futureFreq;
      Utility.ftosamp(freq) => futureBuffersize;
    }

    // only to be called from PROCESSTHREAD
    fun void updateFreq(){
      futureBuffersize => realBuffersize;
      realBuffersize * 2 => fft.size;
      futureFreq => wt.freq;
      float tmp[realBuffersize];
      tmp @=> waveresult;
      wt.setTable(waveresult);
    }

    fun void setPhaseOn(int bool){
      bool => realPhaseon;
    }
}
