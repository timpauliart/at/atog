public class FluxFollower extends Chubgraph
{
  // patch
  inlet => FFT fft =^ Flux flux => blackhole;
  Step dc => Envelope env => outlet;
  // set parameters
  1024 => fft.size;
  Windowing.hann(fft.size()) => fft.window;
  1 => dc.next;
  fft.size()::samp => env.duration;
  float _fluxresult[];


  // spork
  spork ~ pitchfollower_process();

  fun void pitchfollower_process(){
    while (true){
      flux.upchuck().fvals() @=> _fluxresult;
      _fluxresult[0] => env.target;;
      fft.size()::samp => now;
    }
  }
}
