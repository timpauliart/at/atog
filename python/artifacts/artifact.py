from ..notemaker import Art
from ..utility import insert_list


class Artifact:
    def __init__(self, notemaker, string):
        self.notemaker = notemaker
        self.string = string
        self.stack = None

    def instanceof(self):
        return None

    def get(self):
        return None

    def put(self):
        return None

    def make_measure(self, before, after, transform=lambda x: x):
        arglists = self.atoms[self.string]
        phrase = self.notemaker.make_phrase(transform(arglists))
        measure = self.notemaker.make_measure(phrase)

        # assert only one artifact
        if self.stack != None:
            assert (len(self.stack) < 2)

        # artifact ist not the same
        if before.instanceof() != self.instanceof() and self.get() != None:
            # artifact ist not on the stack
            if not before.stack or (before.stack[-1].instanceof() != self.instanceof()):
                insert_list(measure, self.get())
                args = (0, (1 / 8), Art.REST)
                switch = self.notemaker.make_notes(*args)
                insert_list(measure, switch)

        if self.stack != None:
            if self.stack:
                if after.stack != None:
                    # keep
                    after.stack.append(self.stack.pop())
                elif after.instanceof() != self.instanceof() and after.instanceof() != self.stack[-1].instanceof():
                    tmp = self.stack.pop()
                    if tmp.instanceof() == 'bow':
                        measure.extend(tmp.put(elecoff=False))
                    else:
                        measure.extend(tmp.put())
        else:
            if after.instanceof() != self.instanceof():
                if after.stack != None:
                    # keep
                    after.stack.append(self)

                elif self.put() != None:
                    # put
                    measure.extend(self.put())

        return measure
