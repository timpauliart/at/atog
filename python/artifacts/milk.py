from ..notemaker import Art, Boardh, Bowh
from .artifact import Artifact


class Milk(Artifact):
    def instanceof(self):
        return 'milk'

    def get(self):
        result = []
        pick = (0, (1 / 8), Art.GM)
        result.extend(self.notemaker.make_notes(*pick))
        pos = (0, (1 / 8), Art.DOWN)
        result.extend(self.notemaker.make_notes(*pos))
        return result

    def put(self):
        result = []
        pos = (0, (1 / 8), Art.UP)
        result.extend(self.notemaker.make_notes(*pos))
        slam = (0, (1 / 8), Art.PM)
        result.extend(self.notemaker.make_notes(*slam))
        return result


class MilkOn(Milk):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {3: (((3, (6, 8), Art.REST, 'x', None, 'mp'), {}),),
                      2: (((2, (1, 8), Art.REST, 0, None, 'p'), {}),),
                      1: (((1, (4, 8), Art.REST, 0, None, 'mp', None, None, True), {}),)}


class MilkOff(Milk):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
