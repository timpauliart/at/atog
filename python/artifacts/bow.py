import copy as cp

from ..notemaker import Art, Boardh, Bowh
from .artifact import Artifact


class Bow(Artifact):
    def instanceof(self):
        return 'bow'

    def make_measure(self, before, after):
        if before.instanceof() == self.instanceof():
            def on(ls):
                return ls
        else:
            def on(ls):
                result = cp.deepcopy(ls)
                result[0][1]['elec'] = 'on'
                return result
        return super().make_measure(before, after, on)


class SlowBow(Bow):
    def instanceof(self):
        return 'bow'

    def get(self):
        result = []
        pick = (0, (1 / 8), Art.GB)
        result.extend(self.notemaker.make_notes(*pick))
        pos = (0, (1 / 8), Art.DOWN)
        result.extend(self.notemaker.make_notes(*pos))
        return result

    def put(self, elecoff=True):
        result = []
        pos = (0, (1 / 8), Art.UP, 'x', None,
               None, None, None, False, 1, 'off')
        result.extend(self.notemaker.make_notes(*pos))
        if elecoff:
            slam = (0, (1 / 8), Art.PB)
        else:
            slam = (0, (1 / 8), Art.PB)
        result.extend(self.notemaker.make_notes(*slam))
        return result


class FastBow(Bow):
    def get(self):
        args = (0, (1 / 8), Art.GB)
        return self.notemaker.make_notes(*args)

    def put(self, elecoff=True):
        if elecoff:
            args = (0, (1 / 8), Art.PB, 'x', None,
                    None, None, None, False, 1, 'off')
        else:
            args = (0, (1 / 8), Art.PB)

        return self.notemaker.make_notes(*args)


class FastLegno(FastBow):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {2: (((0, (1, 8), Art.REST), {}),)}


class SlowLegno(SlowBow):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {4: ((((4, 3), (1, 8), Art.CLB, 0, None, 'mf'), {}),),
                      3: ((((3, 2), (1, 16), Art.REST, 0, None, None, None, Boardh.HARM), {}),
                          (((3, 2), (2, 16), Art.CLB, 5, None,
                            'mp', None, Boardh.HARM), {}),
                          (((3, 2), (1, 16), Art.REST, 0, None, None, None, Boardh.HARM), {})),
                      1: (((1, (1, 8), Art.CLB, 0, None, 'p'), {}),)}


class Hair(SlowBow):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {4: ((((4, 3), (5, 8), Art.ARCO, 0, None, 'ff'), {}),),
                      3: ((((3, 2), (3, 8), Art.ARCO, 8, None, 'ff', Bowh.TREM16, Boardh.HARM), {}),),
                      2: ((((2, 1), (1, 8), Art.ARCO, 5, None, 'ff'), {}),),
                      1: ((((4, 3), (7, 8), Art.ARCO, 8, 'overpressure', 'ff'), {}),)}
