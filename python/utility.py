import copy as cp

import abjad as aj


def post(arg):
    print(arg)
    return(arg)


def flatten(ls):
    return [x for y in ls for x in y]


def swap(ls, i1, i2):
    result = list(ls)
    result[i1], result[i2] = result[i2], result[i1]
    return result


def repeat(ls, start, end, n):
    rev = start > end
    if rev:
        s, e = end, start
    else:
        s, e = start, end
    # extract first
    src = ls[s:e]

    first = ls[:s]
    last = ls[e:]
    # reverse
    if rev:
        src = list(reversed(src))
    # repeat
    loop = [cp.deepcopy(src) for i in range(n)]
    loop = flatten(loop)
    result = first + loop + last
    return result

def insert_list(measure, ls):
    for i, e in enumerate(ls):
        measure.insert(i, e)
    return measure
